#!yaml|gpg

## Specify the password for the ADMIN user on your IPMI devices using the gpg key located at /root/key.gpg
## As of right now, there isn't really a way to autodiscover and figure out which BMC belongs to who when
## using a non-managed DHCP server (a managed one will be available in a later release and this option will
## become more flexible.  For now, use the same strong password on all of your BMCs if you want to use this
## feature.  Ensure that your BMCs *cannot* access anything outside of the OOB network, and the only device
## that can reach inside the OOB network is your salt master.

bmc_password: |
  -----BEGIN PGP MESSAGE-----

  hF4DpVF6xD6wXegSAQdAMsFIJBA4RyKLlcEV60fV82zLJYXed+LV/+sJwcv8Ekow
  SBnDWizOhKH3ZMKjR4BxfTY9au/CJNQOx6fhqXtEs11vjTB4X9OW8IYmKuX1WGeV
  0ksB0fHPr0uJwaBrsdDxdJrrP+aCCE/4L3PfFbaJcg28+qBKvLD55GNu5CesTIIa
  vOzcS+kahKP0YV+5Nqs8oRoGaK4xPR9dw6Hlia8=
  =+6rE
  -----END PGP MESSAGE-----
